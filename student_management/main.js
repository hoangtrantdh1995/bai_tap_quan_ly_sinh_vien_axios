// tạo biến BASE_URL
const BASE_URL = "https://633ec0720dbc3309f3bc56fd.mockapi.io";
// bật , tắt loading
var turnOnLoading = function () {
    document.getElementById("loading").style.display = "flex";
}
var turnOffLoading = function () {
    document.getElementById("loading").style.display = "none";
}

// lấy danh sách sinh viên service

var getStudentList = function () {
    turnOnLoading();
    axios({
        url: `${BASE_URL}/studentManagementAxios`,
        method: "GET",
    })
        .then(function (response) {
            renderStudentList(response.data);
            turnOffLoading();
            // dssv = response.data;
        })
        .catch(function (error) {
            turnOffLoading();
            console.log('error: ', error);
        });
};
// chạy lần đầu khi load trang
getStudentList();

// render danh sách sinh viên
var renderStudentList = function (listStudent) {
    // contentHTML là 1 chuổi chứa các thẻ tr sau này sẽ innerHTLM vào thẻ tbody
    var contentHTML = "";
    // dùng forEach tạo ra mảng
    listStudent.forEach(function (student) {
        var studentObj = new studentSV(
            student.code,
            student.name,
            student.email,
            student.password,
            student.math,
            student.physics,
            student.chemistry,
        );
        console.log('studentObj: ', studentObj);
        contentHTML += `<tr>
                        <td>${studentObj.code}</td>
                        <td>${studentObj.name}</td>
                        <td>${studentObj.email}</td>
                        <td>${studentObj.averageScore()}</td>
                        <td>
                        <button onclick="getStudentDetails(${studentObj.code})" class="btn btn-primary">Sửa</button>
                        <button onclick="deleteStudent(${studentObj.code})" class="btn btn-danger">Xóa</button>
                        </td>
                        </tr>`
    });
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xóa sinh viên
var deleteStudent = function (codeStudent) {
    axios({
        url: `${BASE_URL}/studentManagementAxios/${codeStudent}`,
        method: "DELETE",
    })
        .then(function (res) {
            // gọi lại API lấy danh , sau khi xóa thành công
            getStudentList();
            // dssv = response.data;

            // thông báo thành công
            Swal.fire("Xóa thành công")
        })
        .catch(function (err) {
            console.log('err: ', err);
            Swal.fire("Xóa thất bại")
        });
};

// thêm sinh viên
var moreStudents = function () {
    var student = getInformationFromForm();

    // validate here
    // return (dung xu ly)
    // pass ->
    // send data to api
    axios({
        url: `${BASE_URL}/studentManagementAxios`,
        method: "POST",
        data: student,
    })
        .then(function (res) {
            // gọi lại API lấy danh , sau khi xóa thành công
            getStudentList();
            // dssv = response.data;

            // thông báo thành công
            Swal.fire("Thêm thành công")

            resetForm();
        })
        .catch(function (err) {
            console.log('err: ', err);
            Swal.fire("Thêm thất bại")
        });
};

// lấy thông tin chi tiết sinh viên
var getStudentDetails = function (codeStudent) {
    axios({
        url: `${BASE_URL}/studentManagementAxios/${codeStudent}`,
        method: "PUT",
    })
        .then(function (res) {
            // show thông tin lên form
            showInformationFromForm(res.data);
        })
        .catch(function (err) {
            console.log('err: ', err);
            Swal.fire("Thêm thất bại")
        });
    // axios
    //     .get(`${BASE_URL}/studentManagementAxios/${codeStudent}`)
    //     .then(function (res) {
    //         //  gọi hàm show thông tin lên form
    //         showInformationFromForm();
    //     })
    //     .catch(function (err) {
    //         console.log('err: ', err);
    //     });
};

// cập nhật sinh viên
var updateStudents = function () {
    var student = getInformationFromForm();
    axios({
        url: `${BASE_URL}/studentManagementAxios/${student.code}`,
        method: "PUT",
        data: student,
    })
        .then(function (res) {
            // reload list
            getStudentList();
            Swal.fire("Cập nhật thành công")
            resetForm();
        })
        .catch(function (err) {
            Swal.fire("Cập nhật thất bại")
        });

};
