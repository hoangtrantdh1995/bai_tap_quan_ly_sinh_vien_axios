function emptyTest(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).innerHTML = "Trường này không được để rỗng";
        return false;
    } else {
        document.getElementById(idError).innerHTML = "";
        return true;
    };
    console.log("123");
};

function checkStudentCode(idSv, listSv, idError) {
    var index = listSv.findIndex(function (show) {
        return show.code == idSv;
    });
    if (index == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = "Mã sinh viên đã tồn tại";
        return false;
    }
};

function checkEmail(value, idError) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (!isEmail) {
        document.getElementById(idError).innerText = "Email không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
};

function checkPassword(value, idError) {
    const re =
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
    var isPassword = re.test(value);
    if (!isPassword) {
        document.getElementById(idError).innerText = "Mật khẩu gồm: chữ hoa, chữ thường, số và ít nhất 8 ký tự trở lên ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
};

