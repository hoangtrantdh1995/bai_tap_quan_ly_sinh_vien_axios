// lấy thông tin từ user 
function getInformationFromForm() {
    var studentCode = document.getElementById("txtMaSV").value.trim();
    var studentName = document.getElementById("txtTenSV").value.trim();
    var email = document.getElementById("txtEmail").value.trim();
    var password = document.getElementById("txtPass").value.trim();
    var mathScore = document.getElementById("txtDiemToan").value.trim();
    var physicsScore = document.getElementById("txtDiemLy").value.trim();
    var chemistryScore = document.getElementById("txtDiemHoa").value.trim();

    // tạo ofject từ thông tin lấy từ form
    var student = new studentSV(studentCode, studentName, email, password, mathScore, physicsScore, chemistryScore);
    return student;
};

function showInformationFromForm(student) {
    document.getElementById("txtMaSV").value = student.code;
    document.getElementById("txtTenSV").value = student.name;
    document.getElementById("txtEmail").value = student.email;
    document.getElementById("txtPass").value = student.password;
    document.getElementById("txtDiemToan").value = student.math;
    document.getElementById("txtDiemLy").value = student.physics;
    document.getElementById("txtDiemHoa").value = student.chemistry;
};

// reset form
function resetForm() {
    document.getElementById("formQLSV").reset();
};

