function studentSV(_code, _name, _email, _password, _mathScore, _physicsScore, _chemistryScore) {
    this.code = _code;
    this.name = _name;
    this.email = _email;
    this.password = _password;
    this.math = _mathScore;
    this.physics = _physicsScore;
    this.chemistry = _chemistryScore;
    this.averageScore = function () {
        var mediumScore = (this.math * 1 + this.physics * 1 + this.chemistry * 1) / 3
        // toFixed : làm trong 1 chữ số
        return mediumScore.toFixed(1);
    };
};